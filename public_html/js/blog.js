 /* global Backendless */
$(function () {
    var  APPLICATION_ID = "6C4A06B9-6381-A124-FF0C-3C6FBB6EE500", 
         SECRET_KEY = "6C90C485-33F5-BF00-FFD3-657950B4B600", 
         VERSION = "v1"; 
           
    Backendless.initApp(APPLICATION_ID, SECRET_KEY, VERSION); 
    
    var postCollection = Backendless.Persistance.of(Posts).find();
    
    console.log(postCollection); 
    
    var wrapper = {
        post: postsCollection.data
   };
    
    Handlebars.registerHelper('format', function (time) {
        return moment(time).format("dddd, MMMM Do YYYY");
    }); 

    var blogScript = $("#blogs-template").html();
    var blogTemplate = Handlebars.compile(blogScript);
    var blogHTML = blogTemplate(wrapper); 

    $('.main-container').html(blogHTML)
    
});  

function Posts(args){
    args = args || {}; 
    this.title = args.title || "";
    this.content = args.content || "";
    this.authorEmail = args.authorEmail || ""; 
}

